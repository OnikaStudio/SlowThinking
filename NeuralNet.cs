﻿using System.Collections.Generic;
using UnityEngine;

namespace SlowThinking
{
    [System.Serializable]
    public class NeuralNet
    {
        public double fitness;
        public double unfixedFitness;
        int m_NumInputs;

        int m_NumOutputs;

        int m_NumHiddenLayers;

        int m_NeuronsPerHiddenLyr;
        
        //storage for each layer of neurons including the output layer
        public List<NeuronLayer> m_vecLayers;
        
	    public NeuralNet(AIParams aiParams)
        {
            m_NumInputs = aiParams.nbInputs;
            m_NumOutputs = aiParams.nbOutputs;
            m_NumHiddenLayers = aiParams.numHidden;
            m_NeuronsPerHiddenLyr = aiParams.neuronsPerHiddenLayer;
            fitness = aiParams.dStartEnergy;
            unfixedFitness = aiParams.dStartEnergy;
            CreateNet();
        }

        public void Reset(AIParams aiParams)
        {
            fitness = aiParams.dStartEnergy;
            unfixedFitness = aiParams.dStartEnergy;
        }

        public void CreateNet()
        {
            m_vecLayers = new List<NeuronLayer>();
            if (m_NumHiddenLayers > 0)
            {
                //create first hidden layer
                m_vecLayers.Add(new NeuronLayer(m_NeuronsPerHiddenLyr, m_NumInputs));

                for (int i = 0; i < m_NumHiddenLayers - 1; ++i)
                {

                    m_vecLayers.Add(new NeuronLayer(m_NeuronsPerHiddenLyr, m_NeuronsPerHiddenLyr));
                }

                //create output layer
                m_vecLayers.Add(new NeuronLayer(m_NumOutputs, m_NeuronsPerHiddenLyr));
            }

            else
            {
                //create output layer
                m_vecLayers.Add(new NeuronLayer(m_NumOutputs, m_NumInputs));
            }
        }

        List<double> weights;
        //gets the weights from the NN
       public List<double> GetWeights()
        {
            //this will hold the weights
            if(weights != null)
            {
                weights.Clear();
            }
            else
            {
                weights = new List<double>();
            }

            //for each layer
            for (int i = 0; i < m_NumHiddenLayers + 1; ++i)
            {

                //for each neuron
                for (int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j)
                {
                    //for each weight
                    for (int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k)
                    {
                        weights.Add(m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k]);
                    }
                }
            }

            return weights;
        }

        //returns total number of weights in net
        public int GetNumberOfWeights() {

            int weights = 0;

            //for each layer
            for (int i = 0; i < m_NumHiddenLayers + 1; ++i)
            {

                //for each neuron
                for (int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j)
                {
                    //for each weight
                    for (int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k)

                        weights++;
                }
            }

            return weights;
        }

        //replaces the weights with new ones
        public void PutWeights(ref List<double> weights)
        {
            int cWeight = 0;

            //for each layer
            for (int i = 0; i < m_NumHiddenLayers + 1; ++i)
            {

                //for each neuron
                for (int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j)
                {
                    //for each weight
                    for (int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k)
                    {
                        m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] = weights[cWeight++];
                    }
                }
            }

            return;
        }
        List<double> outputs;
        //calculates the outputs from a set of inputs
        public List<double> Update(ref List<double> inputs, AIParams aiParams)
        {
            //stores the resultant outputs from each layer
            if(outputs != null)
            {
                outputs.Clear();
            }
            else
            {
                outputs = new List<double>();
            }
            
            int cWeight = 0;

            //first check that we have the correct amount of inputs
            if (inputs.Count != m_NumInputs)
            {
                //just return an empty vector if incorrect.
                Debug.Log("Wrong number of inputs in neural net");
                return outputs;
            }
            
            //Debug.Log("nb hidden layers " + m_NumHiddenLayers);
            //For each layer....
            for (int i = 0; i < m_NumHiddenLayers + 1; ++i)
            {
                if (i > 0)
                {
                    inputs = new List<double>(outputs);
                }

                outputs.Clear();
                cWeight = 0;
                //Debug.Log("nb neurons" + m_vecLayers[i].m_NumNeurons);
                //for each neuron sum the (inputs * corresponding weights).Throw 
                //the total at our sigmoid function to get the output.
                for (int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j)
                {
                    double netinput = 0;

                    int NumInputs = m_vecLayers[i].m_vecNeurons[j].m_NumInputs;

                    //for each weight
                    for (int k = 0; k < NumInputs - 1; ++k)
                    {
                        //Debug.Log(inputs.Count + " " + cWeight);
                        //sum the weights x inputs
                        netinput += m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] * inputs[cWeight++];
                    }

                    //add in the bias
                    netinput += m_vecLayers[i].m_vecNeurons[j].m_vecWeight[NumInputs - 1] * aiParams.dBias;

                    
                    //we can store the outputs from each layer as we generate them. 
                    //The combined activation is first filtered through the sigmoid 
                    //function
                    outputs.Add(Sigmoid(netinput, aiParams.dActivationResponse));

                    cWeight = 0;
                }
            }

            return outputs;
        }

        //sigmoid response curve
        double Sigmoid(double netinput, double response)
        {
            return (1 / (1 + Mathf.Exp((float)-netinput / (float)response)));
        }

        List<int> SplitPoints;
        //calculate the points which define the individual neurons
        public List<int> CalculateSplitPoints()
        {
            if(SplitPoints != null)
            {
                SplitPoints.Clear();
            }
            else
            {
                SplitPoints = new List<int>();
            }

            int WeightCounter = 0;

            //for each layer
            for (int i = 0; i < m_NumHiddenLayers + 1; ++i)
            {
                //for each neuron
                for (int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j)
                {
                    //for each weight
                    for (int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k)
                    {
                        ++WeightCounter;
                    }

                    SplitPoints.Add(WeightCounter - 1);
                }
            }

            return SplitPoints;
        }
    }
}