﻿using UnityEngine;

namespace SlowThinking
{
    [ExecuteInEditMode]
    public class AIParams : MonoBehaviour
    {
        public int nbInputs;
        public int nbOutputs;
        public bool loadFromSettingsFile = false;
        public int numHidden = 1;
        public int neuronsPerHiddenLayer= 10;
        //the bias should always be -1 
        [HideInInspector]
        public double dBias = -1;
        public double dActivationResponse = 1;
        public double dMaxPerturbation = 0.3;
        public int iNumElite = 4;
        public int iNumCopiesElite = 1;
        public double dCrossoverRate = 0.7;
        public double dMutationRate = 0.1;
        public int iNumTicks = 10;
        public int nbNets = 10;
        public double dStartEnergy = 20;
        public bool useTornamentSelection = false;
        public int nbTournamentPlayers = 5;



        public void LoadFromSettings(Settings settings)
        {
            if (!loadFromSettingsFile || settings == null)
                return;
            numHidden = settings.numHidden;
            neuronsPerHiddenLayer = settings.neuronsPerHiddenLayer;
            dActivationResponse = settings.dActivationResponse;
            dMaxPerturbation = settings.dMaxPerturbation;
            iNumElite = settings.iNumElite;
            iNumCopiesElite = settings.iNumCopiesElite;
            dCrossoverRate = settings.dCrossoverRate;
            dMutationRate = settings.dMutationRate;
            iNumTicks = settings.iNumTicks;
            nbNets = settings.nbNets;
            dStartEnergy = settings.dStartEnergy;
            useTornamentSelection = settings.useTornamentSelection;
            nbTournamentPlayers = settings.nbTournamentPlayers;
        }
        public void SaveToSettings(Settings settings)
        {
            if (settings == null)
            {
                return;
            }
            settings.numHidden = numHidden;
            settings.neuronsPerHiddenLayer =  neuronsPerHiddenLayer;
            settings.dActivationResponse = dActivationResponse;
            settings.dMaxPerturbation = dMaxPerturbation;
            settings.iNumElite = iNumElite;
            settings.iNumCopiesElite = iNumCopiesElite;
            settings.dCrossoverRate = dCrossoverRate;
            settings.dMutationRate = dMutationRate;
            settings.iNumTicks = iNumTicks;
            settings.nbNets = nbNets;
            settings.dStartEnergy =  dStartEnergy;
            settings.useTornamentSelection = useTornamentSelection;
            settings.nbTournamentPlayers = nbTournamentPlayers;
        }
        void GetInputsOutputs()
        {
           //do something to calculate nb of inputs and outputs for NNs
        }
        
       
    }
}