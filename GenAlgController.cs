﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace SlowThinking
{
    [System.Serializable]
    public class GenAlgController
    {
        List<Genome> m_vecThePopulation;
        GenAlg m_pGA = null;
        public int nbNets;
        List<NeuralNet> neuralNets;
        //public List<Actor> population;
        //number of weights required for the neural net
        int m_NumWeightsInNN;
        //stores the average fitness per generation for use 
        //in graphing.
        public List<double> m_vecAvFitness;
        //stores the best fitness per generation
        List<double> m_vecBestFitness;
        //cycles per generation
        public int m_iTicks;
        //generation counter
        public int m_iGenerations = 0;
        
        int genAlgID = 0;
        AIParams aiParams;

        public GenAlgController(/*List<Actor> population,*/ AIParams aiParams, int ID)
        {
            this.aiParams = aiParams;
            nbNets = aiParams.nbNets;
            neuralNets = new List<NeuralNet>();
            genAlgID = ID;
            //this.population = population;

            for (int i = 0; i < nbNets; i++)
            {
                neuralNets.Add(new NeuralNet(aiParams));
            }
            //for (int i = 0; i < population.Count; i++)
            //{
            //    population[i].actorMind.genAlgController = this;
            //    population[i].actorMind.NNid = i;
            //}

            //get the total number of weights used in the
            //NN so we can initialise the GA
            m_NumWeightsInNN = neuralNets[0].GetNumberOfWeights();


            //calculate the neuron placement in the weight vector
            List<int> SplitPoints = neuralNets[0].CalculateSplitPoints();

            //get genAlg from file - if null (file doesnt exist) or if the nb of weights is not the same as with the new neurons (something changed in setup), 
            //CREATE NEW
            m_pGA = DeserializeGenAlg();
            if (m_pGA == null)
            {
                Debug.Log("No file found for gen alg ID: " + genAlgID + " ... creating new GenAlg");
                //initialize the Genetic Algorithm class
                m_pGA = new GenAlg(nbNets, aiParams.dMutationRate, aiParams.dCrossoverRate, m_NumWeightsInNN, SplitPoints,
                    aiParams.useTornamentSelection, aiParams.iNumCopiesElite, aiParams.iNumElite, aiParams.nbTournamentPlayers, aiParams.dMaxPerturbation);

               
            }
            else if (m_pGA.GetChromoLength() != m_NumWeightsInNN)
            {
                Debug.Log("Nb of GenAlg chromosones dont match nb Neural Network weights for ID: " + genAlgID + " ... creating new GenAlg");
                m_pGA = new GenAlg(nbNets, aiParams.dMutationRate, aiParams.dCrossoverRate, m_NumWeightsInNN, SplitPoints,
                   aiParams.useTornamentSelection, aiParams.iNumCopiesElite, aiParams.iNumElite, aiParams.nbTournamentPlayers, aiParams.dMaxPerturbation);
            }
            else if (m_pGA.GetPopulationSize() != nbNets)
            {
                Debug.Log("GenAlg population doesnt match nb neural nets for ID: " + genAlgID + " ... creating new GenAlg");
                m_pGA = new GenAlg(nbNets, aiParams.dMutationRate, aiParams.dCrossoverRate, m_NumWeightsInNN, SplitPoints,
                   aiParams.useTornamentSelection, aiParams.iNumCopiesElite, aiParams.iNumElite, aiParams.nbTournamentPlayers, aiParams.dMaxPerturbation);
            }
            else
            {
                m_pGA.Reinit(aiParams.dMutationRate, aiParams.dCrossoverRate, aiParams.useTornamentSelection, aiParams.iNumCopiesElite, aiParams.iNumElite, aiParams.nbTournamentPlayers, aiParams.dMaxPerturbation);
                Debug.Log("Used saved file: " + "genAlg" + genAlgID + ".txt");
            }
            m_iTicks = m_pGA.nbTicks;
            //Otherwise used this saved genAlg 


            //Get the weights from the GA and insert into the sweepers brains
            m_vecThePopulation = m_pGA.GetChromos();
            for (int i = 0; i < nbNets; i++)
            {
                neuralNets[i].PutWeights(ref m_vecThePopulation[i].vecWeights);
            }
            m_vecAvFitness = new List<double>();
            m_vecBestFitness = new List<double>();
            
        }

        GenAlg DeserializeGenAlg()
        {
            if(File.Exists(Application.dataPath + "/../genAlgs/genAlg" + genAlgID + ".txt"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(Application.dataPath + "/../genAlgs/genAlg" + genAlgID + ".txt", FileMode.Open, FileAccess.Read);
                return (GenAlg)formatter.Deserialize(stream);
            }
            else
            {
                return null;
            }
        }

        public void SerializeGenAlg()
        {
            if(!Directory.Exists(Application.dataPath + "/../genAlgs/"))
            {
                Directory.CreateDirectory(Application.dataPath + "/../genAlgs/");

            }
            if (m_pGA != null)
            {
                //saving the nb of ticks so that I can stop at any time... 
                m_pGA.nbTicks = m_iTicks;
                BinaryFormatter formatter = new BinaryFormatter();
                    Stream stream = new FileStream(Application.dataPath + "/../genAlgs/genAlg" + genAlgID + ".txt", FileMode.Create, FileAccess.Write);
                    formatter.Serialize(stream, m_pGA);
            }
        }
        public bool buildingPopulation = false;

        public void UpdateTick(AIParams aiParams)
        {
            m_iTicks += 1;

            if (!buildingPopulation && (m_iTicks > aiParams.iNumTicks * nbNets))
            {
                Epoch();
            }
        }

        public int GetCurrentNNID()
        {
            return m_iTicks % neuralNets.Count;
        }
        public NeuralNet GetNeuralNet(int id)
        {
            return neuralNets[id];
        }
        public void Epoch()
        {
            buildingPopulation = true;
            
            //info for graphs
            m_vecAvFitness.Add(m_pGA.AverageFitness());
            m_vecBestFitness.Add(m_pGA.BestFitness());


            //increment the generation counter
            ++m_iGenerations;

            //reset cycles
            m_iTicks = 0;

            //run the GA to create a new population
            m_vecThePopulation = m_pGA.Epoch(ref m_vecThePopulation);

            Debug.Log("Population: " + m_vecThePopulation.Count + " vs number nets init:" + nbNets);
            //insert the new (hopefully)improved brains
            for (int i = 0; i < nbNets; ++i)
            {
                neuralNets[i].PutWeights(ref m_vecThePopulation[i].vecWeights);

                neuralNets[i].Reset(aiParams);
            }
            buildingPopulation = false;
        }

        
        public void UpdateFitness(int id, double fitness)
        {
            m_vecThePopulation[id].dFitness = neuralNets[id].fitness;
        }
    }

}