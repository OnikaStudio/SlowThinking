﻿using System.IO;
using UnityEngine;

public class XMLSettings
{
    public Settings settings;
    public void LoadSettings()
    {
        if (File.Exists(Application.dataPath + "/../settings.xml"))
        {
            settings = XMLSerializer.Deserialize<Settings>(Application.dataPath + "/../settings.xml");
        }
        else
        {
            settings = new Settings();
            XMLSerializer.Serialize(settings, Application.dataPath + "/../settings.xml");
        }
    }

    public void SaveSettings(SlowThinking.AIParams aiParams)
    {
        aiParams.SaveToSettings(settings);
        if (settings != null)
        {
            XMLSerializer.Serialize(settings, Application.dataPath + "/../settings.xml");
        }
        else
        {
            settings = new Settings();
            XMLSerializer.Serialize(settings, Application.dataPath + "/../settings.xml");
        }
    }
}

public class Settings
{
    public bool loadAIParamsFromSettings = false;
    public int numHidden = 1;
    public int neuronsPerHiddenLayer = 10;
    public double dActivationResponse = 1;
    public double dMaxPerturbation = 0.3;
    public int iNumElite = 4;
    public int iNumCopiesElite = 1;
    public double dCrossoverRate = 0.7;
    public double dMutationRate = 0.1;
    public int iNumTicks = 10;
    public int nbNets = 10;
    public double dStartEnergy = 20;
    public bool useTornamentSelection = false;
    public int nbTournamentPlayers = 5;
}