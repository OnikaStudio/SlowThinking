Slow Thinking
=======

A simple outline for a feedforward neural network and genetic algorithm in C# for use in Unity. 
-----------

The code is adapted from the C++ examples in Mat Buckland's book AI Techniques for Game Programming. 

I first made this to control the minds of the characters in my realtime-animation artwork, Sunshowers (https://xenoangel.com/sunshowers/). This is a generalised version of the neural network and genetic algorithm. 

Please share and use in your projects if it is helpful. I will continue to push updates when I have then. Hopefully including a little demo of how to use it (as I understand that it is currently not that transparent). 

I also hope to make available much more of my work going forward (most often for Unity). Next up will be an A* system for pathfinding on an animated skinned mesh.. ;) 


About Sunshowers:
=======
Sunshowers is inspired by the opening chapter of Akira Kurosawa's film Dreams which follows a young boy as he explores a forest and stumbles accross a fox wedding (Kitsune no Yomeiri).
The piece explores ideas of animism and techno-animism by assigning life in the form of artificial intelligence to all of the objects, both natural and man-made, within the virtual world. The piece unfolds in real time with the characters themselves deciding which paths they will follow. It is a commission for the Barbican's exhibition "AI: More then Human".


CHECK OUT MY WORK AT https://xenoangel.com
=======