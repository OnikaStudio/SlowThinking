﻿using System.Collections.Generic;

namespace SlowThinking
{
    public class NeuronLayer
    {
        //the number of neurons in this layer
        public int m_NumNeurons;

        //the layer of neurons
        public List<Neuron> m_vecNeurons;

        public NeuronLayer(int NumNeurons, int NumInputsPerNeuron)
        {
            m_vecNeurons = new List<Neuron>();
            m_NumNeurons = NumNeurons;
            for (int i = 0; i < NumNeurons; ++i)
            {
                m_vecNeurons.Add(new Neuron(NumInputsPerNeuron));

            }

        }
    }
}