﻿using System.Collections.Generic;
using UnityEngine;

namespace SlowThinking
{
    public class Neuron
    {
        //the number of inputs into the neuron
        public int m_NumInputs;

        //the weights for each input
        public List<double> m_vecWeight;


        public Neuron(int NumInputs)
        {
            m_vecWeight = new List<double>();
            m_NumInputs = NumInputs + 1;
            //we need an additional weight for the bias hence the +1
            for (int i = 0; i < NumInputs + 1; ++i)
            {
                //set up the weights with an initial random value
                m_vecWeight.Add(Random.Range(-1f, 1f));
            }
        }
    }
}