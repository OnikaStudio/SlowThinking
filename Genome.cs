﻿using System.Collections.Generic;

namespace SlowThinking
{
    [System.Serializable]
    public class Genome
    {
        public List<double> vecWeights;

        public double dFitness;

        public Genome()
        {
            dFitness = 0;
            vecWeights = new List<double>();
        }

        public Genome(List<double> w, double f)
        {
            vecWeights = w;
            dFitness = f;
        }

        ////overload '<' used for sorting
        //public static bool operator <(Genome lhs, Genome rhs)
        //{
        //    return (lhs.dFitness < rhs.dFitness);
        //}
        //public static bool operator >(Genome lhs, Genome rhs)
        //{
        //    return (lhs.dFitness > rhs.dFitness);
        //}
    }
}