﻿using System.Collections.Generic;
using UnityEngine;

namespace SlowThinking
{
    [System.Serializable]
    public class GenAlg
    {
        //this holds the entire population of chromosomes
        List<Genome> m_vecPop;

        //size of population
        int m_iPopSize;

        //amount of weights per chromo
        int m_iChromoLength;

        //this holds the positions of the split points in the genome for use
        //in our modified crossover operator
        List<int> m_vecSplitPoints;

        //total fitness of population
        double m_dTotalFitness;

        //best fitness this population
        double m_dBestFitness;

        //average fitness
        double m_dAverageFitness;

        //worst
        double m_dWorstFitness;

        //keeps track of the best genome
        int m_iFittestGenome;

        //probability that a chromosones bits will mutate.
        //Try figures around 0.05 to 0.3 ish
        double m_dMutationRate;

        //probability of chromosones crossing over bits
        //0.7 is pretty good
        double m_dCrossoverRate;

        //generation counter
        int m_cGeneration;

        bool useTornamentSelection = false;

        int iNumCopiesElite;
        int iNumElite;
        int nbTournamentPlayers;
        double dMaxPerturbation;
        public int nbTicks = 0;


        public bool updatingEpoch = false;

        void Crossover(ref List<double> mum, ref List<double> dad, ref List<double> baby1, ref List<double> baby2)
        {
            //just return parents as offspring dependent on the rate
            //or if parents are the same
            if ((Random.value > m_dCrossoverRate) || (mum == dad))
            {
                baby1 = mum;
                baby2 = dad;

                return;
            }

            //determine a crossover point
            int cp = Random.Range(0, m_iChromoLength - 1);

            //create the offspring
            for (int i = 0; i < cp; ++i)
            {
                baby1.Add(mum[i]);
                baby2.Add(dad[i]);
            }

            for (int i = cp; i < mum.Count; ++i)
            {
                baby1.Add(dad[i]);
                baby2.Add(mum[i]);
            }


            return;
        }
        //this crossover operator only performs crossover at
        //the points which represent the start and end points 
        //of the weights of each neuron in the net.
        void CrossoverAtSplits(ref List<double> mum, ref List<double> dad, ref List<double> baby1, ref List<double> baby2)
        {
            //just return parents as offspring dependent on the rate
            //or if parents are the same
            if ((Random.value > m_dCrossoverRate) || (mum == dad))
            {
                baby1 = mum;
                baby2 = dad;

                return;
            }

            int point = Random.Range(0, m_vecSplitPoints.Count - 2);
            //Debug.Log("Point1 " + point);
            //determine two crossover points
            int cp1 = m_vecSplitPoints[point];
            //Debug.Log(m_vecSplitPoints.Count);
            //Debug.Log(cp1);
            point = Random.Range(point, m_vecSplitPoints.Count - 1);
            //Debug.Log("Point2 " + point + " out of " + m_vecSplitPoints.Count);
            int cp2 = m_vecSplitPoints[point];


            //create the offspring
            for (int i = 0; i < mum.Count; ++i)
            {
                if ((i < cp1) || (i >= cp2))
                {
                    //keep the same genes if outside of crossover points
                    baby1.Add(mum[i]);
                    baby2.Add(dad[i]);
                }

                else
                {
                    //switch over the belly block
                    baby1.Add(dad[i]);
                    baby2.Add(mum[i]);
                }

            }

            return;
        }


        void Mutate(ref List<double> chromo)
        {
            //traverse the chromosome and mutate each weight dependent
            //on the mutation rate
            for (int i = 0; i < chromo.Count; ++i)
            {
                //do we perturb this weight?
                if (Random.value < m_dMutationRate)
                {
                    //add or subtract a small value to the weight
                    chromo[i] += (Random.Range(-1f, 1f) * dMaxPerturbation);
                }
            }
        }

        Genome GetChromoRoulette()
        {
            //generate a random number between 0 & total fitness count
            double Slice = (double)(Random.value) * m_dTotalFitness;

            //this will be set to the chosen chromosome
            Genome TheChosenOne = null;

            //go through the chromosones adding up the fitness so far
            double FitnessSoFar = 0;

            for (int i = 0; i < m_iPopSize; ++i)
            {
                FitnessSoFar += m_vecPop[i].dFitness;

                //if the fitness so far > random number return the chromo at 
                //this point
                if (FitnessSoFar >= Slice)
                {
                    TheChosenOne = m_vecPop[i];

                    break;
                }
            }

            return TheChosenOne;
        }
        Genome TournamentSelection(int N)
        {
            double BestFitnessSoFar = 0;
            int ChosenOne = 0;
            //Select N members from the population at random testing against
            //the best found so far
            for (int i = 0; i < N; ++i)
            {
                int ThisTry = Random.Range(0, m_iPopSize - 1);
                if (m_vecPop[ThisTry].dFitness > BestFitnessSoFar)
                {
                    ChosenOne = ThisTry;
                    BestFitnessSoFar = m_vecPop[ThisTry].dFitness;
                }
            }
            //return the champion
            return m_vecPop[ChosenOne];
        }

        void GrabNBest(int NBest, int NumCopies, ref List<Genome> Pop)
        {
            //add the required amount of copies of the n most fittest 
            //to the supplied vector
            while (NBest-- > 0)
            {
                for (int i = 0; i < NumCopies; ++i)
                {
                    Pop.Add(m_vecPop[(m_iPopSize - 1) - NBest]);
                }
            }
        }

        void FitnessScaleRank()
        {
            const int FitnessMultiplier = 1;

            //assign fitness according to the genome's position on
            //this new fitness 'ladder'
            for (int i = 0; i < m_iPopSize; i++)
            {
                m_vecPop[i].dFitness = i * FitnessMultiplier;
            }

            //recalculate values used in selection
            CalculateBestWorstAvTot();
        }

        void CalculateBestWorstAvTot()
        {
            m_dTotalFitness = 0;

            double HighestSoFar = 0;
            double LowestSoFar = 9999999;

            for (int i = 0; i < m_iPopSize; ++i)
            {
                //update fittest if necessary
                if (m_vecPop[i].dFitness > HighestSoFar)
                {
                    HighestSoFar = m_vecPop[i].dFitness;

                    m_iFittestGenome = i;

                    m_dBestFitness = HighestSoFar;
                }

                //update worst if necessary
                if (m_vecPop[i].dFitness < LowestSoFar)
                {
                    LowestSoFar = m_vecPop[i].dFitness;

                    m_dWorstFitness = LowestSoFar;
                }

                m_dTotalFitness += m_vecPop[i].dFitness;

            }//next chromo

            m_dAverageFitness = m_dTotalFitness / m_iPopSize;
        }

        void Reset()
        {
            m_dTotalFitness = 0;
            m_dBestFitness = 0;
            m_dWorstFitness = 9999999;
            m_dAverageFitness = 0;
        }




        public GenAlg(int popsize,
                     double MutRat,
                     double CrossRat,
                     int numweights,
                     List<int> splits,
                     bool useTornamentSelection,
                     int iNumCopiesElite,
                     int iNumElite,
                     int nbTournamentPlayers,
                     double dMaxPerturbation)
        {
            m_iPopSize = popsize;
            m_dMutationRate = MutRat;
            m_dCrossoverRate = CrossRat;
            m_iChromoLength = numweights;
            m_dTotalFitness = 0;
            m_cGeneration = 0;
            m_iFittestGenome = 0;
            m_dBestFitness = 0;
            m_dWorstFitness = 99999999;
            this.useTornamentSelection = useTornamentSelection;
            this.iNumCopiesElite = iNumCopiesElite;
            this.iNumElite = iNumElite;
            this.nbTournamentPlayers = nbTournamentPlayers;
            this.dMaxPerturbation = dMaxPerturbation;

            m_dAverageFitness = 0;
            m_vecSplitPoints = splits;
            m_vecPop = new List<Genome>();
            //initialise population with chromosomes consisting of random
            //weights and all fitnesses set to zero
            for (int i = 0; i < m_iPopSize; ++i)
            {
                m_vecPop.Add(new Genome());

                for (int j = 0; j < m_iChromoLength; ++j)
                {
                    m_vecPop[i].vecWeights.Add(Random.Range(-1f, 1f));
                }
            }
        }
        /// <summary>
        /// Used to reinitialise after loading saved genalg
        /// </summary>
        /// <param name="MutRat"></param>
        /// <param name="CrossRat"></param>
        /// <param name="useTornamentSelection"></param>
        /// <param name="iNumCopiesElite"></param>
        /// <param name="iNumElite"></param>
        /// <param name="nbTournamentPlayers"></param>
        /// <param name="dMaxPerturbation"></param>
        public void Reinit(double MutRat,
                     double CrossRat,
                     bool useTornamentSelection,
                     int iNumCopiesElite,
                     int iNumElite,
                     int nbTournamentPlayers,
                     double dMaxPerturbation)
        {
            m_dMutationRate = MutRat;
            m_dCrossoverRate = CrossRat;
            this.useTornamentSelection = useTornamentSelection;
            this.iNumCopiesElite = iNumCopiesElite;
            this.iNumElite = iNumElite;
            this.nbTournamentPlayers = nbTournamentPlayers;
            this.dMaxPerturbation = dMaxPerturbation;
        }

        List<Genome> vecNewPop;
        //this runs the GA for one generation.
        public List<Genome> Epoch(ref List<Genome> old_pop)
        {
            updatingEpoch = true;
            //assign the given population to the classes population
            m_vecPop = new List<Genome>(old_pop);
            //m_vecPop = old_pop;
             //reset the appropriate variables
             Reset();

            //sort the population (for scaling and elitism)
            //sort(m_vecPop.begin(), m_vecPop.end());
            //m_vecPop.Sort();

            m_vecPop.Sort(delegate (Genome lhs, Genome rhs)
            {
            if (lhs.dFitness > rhs.dFitness) return 1;
                else if (lhs.dFitness < rhs.dFitness) return -1;
                else return 0;
            });
            //Debug.Log("pop size " + m_iPopSize);
            //calculate best, worst, average and total fitness
            CalculateBestWorstAvTot();

            //create a temporary vector to store new chromosones
            if (vecNewPop != null)
            {
                vecNewPop.Clear();
            }
            else
            {
                vecNewPop = new List<Genome>();
            }

            //Now to add a little elitism we shall add in some copies of the
            //fittest genomes. Make sure we add an EVEN number or the roulette
            //wheel sampling will crash
            if ((iNumCopiesElite *iNumElite % 2) == 0)
            {
                GrabNBest(iNumElite, iNumCopiesElite, ref vecNewPop);
            }


            //now we enter the GA loop

            //repeat until a new population is generated
            while (vecNewPop.Count < m_iPopSize)
            {
                Genome mum = null;
                Genome dad = null;
                if (useTornamentSelection)
                {
                    mum = TournamentSelection(nbTournamentPlayers);
                    dad = TournamentSelection(nbTournamentPlayers);

                }
                else
                {
                    //grab two chromosones
                    mum = GetChromoRoulette();
                    dad = GetChromoRoulette();
                }
              

                //create some offspring via crossover
                List<double> baby1 = new List<double>(), baby2 = new List<double>();

                //this isnt working for some reason
                CrossoverAtSplits(ref mum.vecWeights, ref dad.vecWeights, ref baby1, ref baby2);
                //Crossover(ref mum.vecWeights, ref dad.vecWeights, ref baby1, ref baby2);


                //now we mutate
                Mutate(ref baby1);
                Mutate(ref baby2);

                //now copy into vecNewPop population
                vecNewPop.Add(new Genome(baby1, 0));
                vecNewPop.Add(new Genome(baby2, 0));
            }

            //finished so assign new pop back into m_vecPop
            m_vecPop = vecNewPop;
            updatingEpoch = false;
            return m_vecPop;
        }

        public List<Genome> GetNewPopulation()
        {
            return m_vecPop;
        }

        //-------------------------accessor methods
        public List<Genome> GetChromos() { return m_vecPop; }
        public double AverageFitness() { return m_dTotalFitness / m_iPopSize; }
        public double BestFitness() { return m_dBestFitness; }
        public int GetChromoLength() { return m_iChromoLength; }
        public int GetPopulationSize() { return m_iPopSize; }
    }
}